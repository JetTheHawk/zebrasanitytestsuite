import unittest

from selenium import webdriver
from selenium.webdriver.common.by import By

import user_data
from locators import (MainPageLocators, QuestionsStartPageLocators,
                      VehiclesPageLocators)
from pages import MainPage, OwnershipPage, QuestionStartPage, VehiclesPage
from testCases import test_cases

#TODO: add other happy path tests w/ multiple vehicles
# error test cases 
# submit and confirm ownership data
# submit and confirm driver data
# confirm results

class TestPages(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome()
        #self.driver = webdriver.Firefox()
        self.driver.get("http://www.thezebra.com")

    def test_A_page_load(self):
        print()
        print(str(test_cases(0)))
        page = MainPage(self.driver)
        self.assertTrue(page.check_page_loaded())

    def test_B_submit_zipcode(self):
        print()
        print(str(test_cases(1)))
        page = MainPage(self.driver)
        print(user_data.get_zipcode())
        page.enter_zipcode(user_data.get_zipcode())
        page.click_submit_button()
        self.assertTrue(page.confirm_zip_code_submission())
    
    def test_C_submit_address_and_name(self):
        print()
        print(str(test_cases(2)))
        page = QuestionStartPage(self.driver)
        self.driver.implicitly_wait(2)
        print(user_data.get_address())
        page.enter_address(user_data.get_address())
        page.enter_unit(user_data.get_unit())
        page.enter_first_name(user_data.get_first_name())
        page.enter_last_name(user_data.get_last_name())
        page.enter_birthdate(user_data.get_birthdate())
        page.click_save_continue_button()
        self.assertTrue(page.confirm_address_submission())
    
    def test_D_submit_single_vehicle_data(self):
        print()
        print(str(test_cases(3)))
        page = VehiclesPage(self.driver)
        #page.wait_until_vehicle_year_is_clickable()
        page.enter_vehicle_year(user_data.get_vehicle_year())
        page.enter_vehicle_make(user_data.get_vehicle_make())
        page.enter_vehicle_model(user_data.get_vehicle_model())
        page.enter_vehicle_trim(user_data.get_vehicle_trim())
        self.driver.implicitly_wait(2)
        page.click_save_continue_button()
        self.assertTrue(page.confirm_vehicle_submission())
    
    def test_E_verify_result_data(self):
        print()
        print(str(test_cases(4)))
        #page = ResultsPage(self.driver)
        self.driver.implicitly_wait(1)
        self.assertTrue(True)
    
    @classmethod
    def tearDownClass(self):
        self.driver.close()

if __name__ == "__main__":
    suite = unittest.TestLoader().loadTestsFromTestCase(TestPages)
    unittest.TextTestRunner(verbosity=2).run(suite)
