#TODO add locators for:
  #Secondary zipcode submission elements
  #Navigation to insurance guides, state specific pages, compare companies
  #locators for ownership, drivers, and results pages
  
  # Add platform and browserDriver bootstrap options https://chromedriver.storage.googleapis.com/index.html?path=77.0.3865.40/

class MainPageLocators(object):
  zipCodeField  = "[data-cy='zipcode-form-control']"
  submitButton  = "[data-cy='zipcode-submit-button']"

class QuestionsStartPageLocators(object):
  address       = "garaging_addressInput"
  unit          = "[data-cy='unit']"
  firstname     = "[data-cy='first_name']"
  lastname      = "[data-cy='last_name']"
  birthdate     = "[data-cy='date_of_birth']"
  firstDropdown = "[data-cy='pac_item']"
  saveContinue  = "startSaveBtn" 
  
  
class VehiclesPageLocators(object):
  vehicleYear   = "yearYear-0Input-0"
  vehicleMake   = "[data-cy='make-0Input']"
  vehicleModel  = "[data-cy='model-0Input']"
  vehicleTrim   = "[data-cy='submodel-0Input']"
  saveContinueVehicles = "vehiclesSelectSaveBtn"

class OwnershipPageLocators(object):
  ownRadio   = "ownership-0"

  # DriversPage
  # ResultsPage
