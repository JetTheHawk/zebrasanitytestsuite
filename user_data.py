from operator import itemgetter

#turn into multi dimenstional dictionary for multi-user data DONE
user_data = {}		
user_data["default_user"] = {
	"zipcode": "60048",
	"address": "409 Burdick Street",
	"unit": "",
	"firstName": "Joe",
	"lastName": "Testingham",
	"birthDate":"08161989",
	"vehicleYear": "2016",
	"vehicleMake": "Honda",
	"vehivleModel": "Civic",
	"vehicleTrim": "EX 4dr Sedan",
}

#add usertype parameter DONE
def get_zipcode(user="default_user"):
	return user_data[user]["zipcode"]

def get_address(user="default_user"):
	return user_data[user]["address"]

def get_unit(user="default_user"):
	return user_data[user]["unit"]

def get_first_name(user="default_user"):
	return user_data[user]["firstName"]

def get_last_name(user="default_user"):
	return user_data[user]["lastName"]

def get_birthdate(user="default_user"):
	return user_data[user]["birthDate"]

def get_vehicle_year(user="default_user"):
    return user_data[user]["vehicleYear"]

def get_vehicle_make(user="default_user"):
	return user_data[user]["vehicleMake"]

def get_vehicle_model(user="default_user"):
	return user_data[user]["vehivleModel"]

def get_vehicle_trim(user="default_user"):
	return user_data[user]["vehicleTrim"]

	