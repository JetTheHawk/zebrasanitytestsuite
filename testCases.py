def test_cases(number):
    return testCases[number]

    
testCases = [
    #[severity, description]
    ['Sanity', 'When user goes to main page, page should be loaded'],
    ['Sanity', 'In Main page, when user enters and submits zip code, questions start page should be loaded'],
    ['Sanity', 'In questions start page when user enter address and name info Vehicles Page should be loaded'],
    ['Sanity', 'In Vehicles Page, when user enters vehicle data and submits Ownership page should be loaded'],
    ['Sanity', 'In Ownership Page, when user submits ownership data, drivers page should be loaded'],
    ['Sanity', 'In Drivers Page, when user submits driver info, results page should be loaded'],
]
