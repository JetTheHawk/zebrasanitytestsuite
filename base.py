from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

# basic methods for every single page inherited from Page class

class Page(object):
    def __init__(self, driver, base_url='http://www.zebra.com/'):
        self.base_url = base_url
        self.driver = driver
        self.timeout = 30
 
    def find_element(self, locator):
        return self.driver.find_element(locator)
 
    def open(self,url):
        url = self.base_url + url
        self.driver.get(url)
    
    #will wait with a timeout of 5 for locator passed in, only supports ID
    def wait_until_element_is_present(self, findBy, locator):
        self.driver.implicitly_wait(2)
        if findBy=="ID":
            WebDriverWait(self.driver,7).until((EC.element_to_be_clickable((By.ID, locator))))
        elif findBy=="CSS":
            WebDriverWait(self.driver,7).until((EC.element_to_be_clickable((By.CSS_SELECTOR, locator))))
        else:
            print("ERROR: invalid find by paramter passed to waitUntil")
