from selenium import common, webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select, WebDriverWait

import locators
import user_data
from base import Page


class MainPage(Page):
    def __init__(self, driver):
        self.locator = locators.MainPageLocators
        super().__init__(driver)

    def check_page_loaded(self):
        return True if self.driver.find_element_by_css_selector(self.locator.submitButton) else False

    def enter_zipcode(self, zcode):
        zipcodeField = self.driver.find_element_by_css_selector(self.locator.zipCodeField)
        zipcodeField.send_keys(zcode)
        zipcodeField.send_keys(Keys.ENTER)
        return True
        
    def click_submit_button(self):
        self.wait_until_element_is_present("CSS",self.locator.submitButton)
        self.driver.find_element_by_css_selector(self.locator.submitButton).click()
        return True
    
    def confirm_zip_code_submission(self):
        self.wait_until_element_is_present("ID",locators.QuestionsStartPageLocators.address)
        return True

class QuestionStartPage(Page):
    def __init__(self, driver):
        self.locator = locators.QuestionsStartPageLocators
        super().__init__(driver)

    def check_page_loaded(self):
        return True if self.driver.find_element_by_id(self.locator.address) else False
    
    def wait_until_address_is_clickable(self):
        self.wait_until_element_is_present("ID",self.locator.address)

    def enter_address(self, address):
        address_element = self.driver.find_element_by_id(self.locator.address)
        address_element.send_keys(address)
        auto_address_element = self.driver.find_element_by_css_selector(self.locator.firstDropdown)
        auto_address_element.click()
        return True

    def enter_unit(self, unit):
        unit_element = self.driver.find_element_by_css_selector(self.locator.unit)
        unit_element.send_keys(unit)
        unit_element.send_keys(Keys.TAB)
        return True

    def enter_first_name(self, fname):
        firstName_element = self.driver.find_element_by_css_selector(self.locator.firstname)
        firstName_element.send_keys(fname)
        firstName_element.send_keys(Keys.TAB)
        return True

    def enter_last_name(self, lname):
        lastName_element = self.driver.find_element_by_css_selector(self.locator.lastname)
        lastName_element.send_keys(lname)
        lastName_element.send_keys(Keys.TAB)
        return True

    def enter_birthdate(self, bdate):
        birthdate_element = self.driver.find_element_by_css_selector(self.locator.birthdate)
        birthdate_element.send_keys(bdate)
        birthdate_element.send_keys(Keys.TAB)
        self.driver.find_element_by_css_selector(self.locator.lastname).click()
        birthdate_element.click()
        return True

    #Very fragile button click. had to scroll screen and click on other element
    def click_save_continue_button(self):
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        birthdate_element = self.driver.find_element_by_css_selector(self.locator.birthdate)
        birthdate_element.click()
        birthdate_element.send_keys(Keys.TAB)
        birthdate_element.send_keys(Keys.TAB)
        self.driver.find_element_by_css_selector(self.locator.lastname).click()
        self.driver.find_element_by_id("scroll-container").click()
        try:
            self.driver.find_element_by_id(self.locator.saveContinue).click()
        except common.exceptions.ElementClickInterceptedException:
            print("click submit for address info failed. try again")
            self.driver.find_element_by_id(self.locator.saveContinue).click()
        
    
    def confirm_address_submission(self):
        self.wait_until_element_is_present("ID",locators.VehiclesPageLocators.vehicleYear)
        return True
    

#add methods for adding second car
class VehiclesPage(Page):
    def __init__(self, driver):
        self.locator = locators.VehiclesPageLocators
        super().__init__(driver)    
    
    def check_page_loaded(self):
        return True if self.driver.find_element_by_id(self.locator.vehicleYear) else False

    def wait_until_vehicle_year_is_clickable(self):
        self.wait_until_element_is_present("ID",self.locator.vehicleYear)
    
    def enter_vehicle_year(self, year):
        self.wait_until_element_is_present("ID",self.locator.vehicleYear)
        select_year_element = self.driver.find_element_by_id(self.locator.vehicleYear)
        select_year_element.send_keys(year)
        select_year_element.send_keys(Keys.ENTER)
        select_year_element.send_keys(Keys.TAB)
        return True

    def enter_vehicle_make(self, make):
        self.wait_until_element_is_present("CSS",self.locator.vehicleMake)
        make_element = self.driver.find_element_by_css_selector(self.locator.vehicleMake)
        make_element.send_keys(make)
        make_element.send_keys(Keys.ENTER)
        make_element.send_keys(Keys.TAB)
        return True

    def enter_vehicle_model(self, model):
        self.wait_until_element_is_present("CSS",self.locator.vehicleModel)
        model_element = self.driver.find_element_by_css_selector(self.locator.vehicleModel)
        model_element.send_keys(model)
        model_element.send_keys(Keys.ENTER)
        model_element.send_keys(Keys.TAB)
        return True

    def enter_vehicle_trim(self, trim):
        self.wait_until_element_is_present("CSS",self.locator.vehicleTrim)
        trim_element = self.driver.find_element_by_css_selector(self.locator.vehicleTrim)
        trim_element.send_keys(trim)
        trim_element.send_keys(Keys.ENTER)
        trim_element.send_keys(Keys.TAB)
        self.driver.find_element_by_id("scroll-container").click()
        return True

    def click_save_continue_button(self):
        self.wait_until_element_is_present("ID",self.locator.saveContinueVehicles)
        self.driver.find_element_by_id(self.locator.saveContinueVehicles).click()
        return True
    
    def confirm_vehicle_submission(self):
        self.wait_until_element_is_present("ID",locators.OwnershipPageLocators.ownRadio)
        return True

class OwnershipPage(Page):
    def __init__(self, driver):
        self.locator = locators.OwnershipPageLocators
        super().__init__(driver)    
    
    def check_page_loaded(self):
        return True if self.driver.find_element_by_css_selector(self.locator.ownRadio) else False
