# Zebra Compare Test Suite

#### Summary
* Zebra Test Suite is a python unittests selenium suite 
* It verifys core functionaility of the vehicle comparison form

#### Requirements
* python3.5 or greater
* selenium chromedriver install and set in PATH (move to usr/local/bin)
* MACos 10.10+ (Not tested on Windows or Linux)

#### To run the zebra sanity suite, you should type: 
```sh
python testPages.py
```
### Initial Thoughts and Steps
* evaulated site and determined MVP being vehicle compare form submission
* researched virtualenv. Decide re-familiarizing myself would take too long so moved to nice to have status
* determined covering error cases was also a nice to have feature due to time contstraint
* created page objects for each page that will be interacted with
* evaulate element not found errors around the address choice(needing to click autogen address), and autoscroll after brithdate entered.
* Final tests runs

#### NOTES and TODO
* Finish Ownership and Results page objects and test cases
* Submit and continue buttons are quite fragle. had to implement some try/catches and webdriverwaits to ensure clicks happened
* Address and Vehicle dropdown menus cause page to scroll after input entered, this functionality causes the test to become quite fragile. might be worth adding some JS script injection to ensure the auto-scrolls are happening as intended
* Would like implement more base funtions for general cases like Select_and_enter_text and Select_and_choose_selection with waitUntilClickable integrated
* Selenium + Virtual env bootstrapper to keep user from having to alter their enviromnet
* Test results report
* Tagging system so certain sets of tests can be called by passing command line arg. ie. sanity, smoke
* API integration testing using login and form submission APIs
